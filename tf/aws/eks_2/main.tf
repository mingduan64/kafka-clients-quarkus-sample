terraform {
  required_version = "1.5.7"
  # Add any other necessary configurations here
}

provider "aws" {
  region = "ap-southeast-1" # Replace with your desired AWS region
  # Add any other provider configurations here
}

resource "aws_eks_cluster" "demo-eks" {
  name = "demo-eks"
  role_arn = "arn:aws:iam::576694912472:role/eksctl-cluster-2048-game-cluster-ServiceRole-Q2IRQ14MC4N8"
  version = "1.25" # Replace with your desired EKS version

  vpc_config {
    endpoint_private_access = false
    endpoint_public_access = true
    public_access_cidrs = ["0.0.0.0/0"]

    security_group_ids = ["sg-039c9f41568af75f7"]

    subnet_ids = [
      "subnet-017499f6438dc9dc9",
      "subnet-020d28451695f5759",
      "subnet-035ba124f9acaedc6",
      "subnet-0728325c7813add0f",
      "subnet-0b9dd7e80ea87293c",
      "subnet-0e1cc674c61c18a54"
    ]

  }

  # Add any other necessary configurations here
}
