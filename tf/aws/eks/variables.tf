variable "eks_cluster_name" {
  description = "Name of the EKS cluster"
  type        = string
  default     = "my-eks-cluster" # Replace with your desired EKS cluster name
}

variable "eks_cluster_version" {
  description = "Kubernetes version for the EKS cluster"
  type        = string
  default     = "1.21" # Replace with your desired Kubernetes version
}

variable "eks_node_group_name" {
  description = "Name of the EKS node group"
  type        = string
  default     = "my-node-group" # Replace with your desired node group name
}

variable "eks_instance_type" {
  description = "EC2 instance type for worker nodes"
  type        = string
  default     = "m5.large" # Replace with your desired instance type
}

variable "eks_subnet_ids" {
  description = "List of subnet IDs for the EKS cluster"
  type        = list(string)
  default     = ["subnet-xxxxxxxxxxxxx", "subnet-yyyyyyyyyyyyy"] # Replace with your subnet IDs
}

variable "eks_cluster_tags" {
  description = "Tags to apply to the EKS cluster"
  type        = map(string)
  default     = {
    Environment = "Development"
    Owner       = "YourName"
  }
}

variable "aws_region" {
  description = "AWS region for ECR"
  type        = string
  default     = "ap-southeast-1" # Replace with your desired AWS region
  sensitive   = true
}

variable "role_names" {
  description = "List of EKS roles names"
  type        = list(string)
  default     = ["eks-fargate-execution-role"] # Replace with your desired repository names
}