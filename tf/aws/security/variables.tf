variable "security_group_name" {
  description = "Name of the security group"
  type        = string
  default     = "example-sg"
}

variable "security_group_description" {
  description = "Description of the security group"
  type        = string
  default     = "Example security group"
}

variable "ssh_ingress_cidr" {
  description = "CIDR block for SSH ingress rule"
  type        = string
  default     = "0.0.0.0/0"
}

variable "http_ingress_cidr" {
  description = "CIDR block for HTTP ingress rule"
  type        = string
  default     = "0.0.0.0/0"
}

# You can define more variables for other ingress rules or configurations as needed.
