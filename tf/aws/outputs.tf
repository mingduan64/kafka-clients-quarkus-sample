# Outputs for ECR module
output "ecr_repository_url" {
  value = module.ecr.ecr_repository_url
}

# Outputs for EKS module
output "eks_cluster_name" {
  value = module.eks.eks_cluster_name
}

output "eks_kubeconfig" {
  value = module.eks.kubeconfig
}

# Outputs for Network module
output "vpc_id" {
  value = module.network.vpc_id
}

output "subnet_ids" {
  value = module.network.subnet_ids
}

# Outputs for Security module
output "iam_role_name" {
  value = module.security.iam_role_name
}

output "security_group_ids" {
  value = module.security.security_group_ids
}

# Outputs for Monitoring module
output "log_group_name" {
  value = module.monitoring.log_group_name
}

output "container_insights_name" {
  value = module.monitoring.container_insights_name
}
