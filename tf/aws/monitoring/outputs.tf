# Output the ARN of the CloudWatch log group for Fargate logs
output "fargate_logs_arn" {
  value = aws_cloudwatch_log_group.fargate_logs.arn
}

# Output the name of the CloudWatch Container Insights configuration
output "container_insights_name" {
  value = aws_cloudwatch_container_insight.fargate_insight.name
}

# Output the status of CloudWatch Container Insights
output "container_insights_status" {
  value = aws_cloudwatch_container_insight.fargate_insight.status
}
