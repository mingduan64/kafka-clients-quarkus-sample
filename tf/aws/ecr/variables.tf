variable "ecr_repository_names" {
  description = "List of ECR repository names"
  type        = list(string)
  default     = ["product-microservices"] # Replace with your desired repository names
}

variable "aws_region" {
  description = "AWS region for ECR"
  type        = string
  default     = "ap-southeast-1" # Replace with your desired AWS region
}
