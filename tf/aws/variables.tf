# Define variables for AWS provider configuration
variable "aws_region" {
  description = "The AWS region where resources will be created."
  type        = string
  default     = "us-east-1" # Replace with your desired default region
}

variable "aws_access_key" {
  description = "The AWS access key for authentication."
  type        = string
  # You can set a default value here or provide it when running Terraform
}

variable "aws_secret_key" {
  description = "The AWS secret key for authentication."
  type        = string
  # You can set a default value here or provide it when running Terraform
}

# Define other variables for your project here
# For example, variables related to your application, ECR, EKS, network, etc.
