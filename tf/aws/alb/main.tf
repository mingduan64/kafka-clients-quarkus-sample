provider "aws" {
  region = "us-west-2" # Specify your desired AWS region
}

resource "aws_security_group" "alb_sg" {
  name        = "my-alb-sg"
  description = "Security group for the ALB"
  vpc_id      = "vpc-xxxxxxxxxxxxxxxxx" # Replace with your VPC ID

  # Define ingress rules as needed (e.g., allow traffic on port 80)
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # Allow traffic from anywhere (for demonstration purposes)
  }

  # Add more ingress rules if necessary
}

resource "aws_lb" "example" {
  name               = "my-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["subnet-xxxxxxxxxxxxxxxxx", "subnet-yyyyyyyyyyyyyyyyy"] # Replace with your subnet IDs

  enable_http2 = true # Enable HTTP/2 for improved performance

  security_groups = [aws_security_group.alb_sg.id]

  enable_deletion_protection = false # Set to true to enable deletion protection
}

resource "aws_lb_listener" "example" {
  load_balancer_arn = aws_lb.example.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      status_code  = "200"
      content      = "Hello, world!"
    }
  }
}

