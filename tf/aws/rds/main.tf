# Define the RDS instance
resource "aws_db_instance" "mydb" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro" # Free Tier eligible instance type
  db_name              = "demo_1"
  multi_az             = false
  username             = "admin"
  password             = "admin_password"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  db_subnet_group_name = aws_db_subnet_group.mydb_subnet_group.name
  publicly_accessible  = true
  # Specify the subnet group directly in the RDS instance

  # Specify the subnet IDs

  # Network Configuration
  vpc_security_group_ids = ["sg-02097dc6b7d4448f3"]
}
# Subnet in ap-southeast-1a


# Define a subnet group for the RDS instance


resource "aws_db_subnet_group" "mydb_subnet_group" {
  name        = "mydb-subnet-group"
  description = "My DB Subnet Group"

  subnet_ids = [
    "subnet-06bb26db2a6e98a97", # Subnet in ap-southeast-1a
    "subnet-07ddd518b2f40a691", # Subnet in ap-southeast-1b
  ]
}


