variable "aws_region" {
  description = "AWS region for ECR"
  type        = string
  default     = "ap-southeast-1" # Replace with your desired AWS region
  sensitive   = true
}