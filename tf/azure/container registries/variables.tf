variable "azure_region" {
  description = "Azure region where resources will be deployed."
  type        = string
  default     = "East Asia"  # Replace with your desired Azure region in Asia
}


variable "client_name" {
  description = "Client name or identifier."
  type        = string
  default     = "myclient"  # Replace with your client name
}

variable "environment" {
  description = "Environment name or identifier."
  type        = string
  default     = "dev"  # Replace with your environment name
}

variable "stack" {
  description = "Stack name or identifier."
  type        = string
  default     = "kafka-clients-quarkus-sample"  # Replace with your stack name
}

variable "location_short" {
  description = "Short code for Azure region"
  type        = string
  default     = "southeastasia"  # Replace with the appropriate short code
}
