module "azure_region" {
  source  = "claranet/regions/azurerm"
  version = "7.0.0"  # Replace with the desired version

  azure_region = var.azure_region
}

module "rg" {
  source  = "claranet/rg/azurerm"
  version = "6.1.0"  # Replace with the desired version
  # Replace with the desired version

  location    = module.azure_region.location
  client_name = var.client_name
  environment = var.environment
  stack       = var.stack
}
#
#module "logs" {
#  source  = "claranet/run/azurerm//modules/logs"
#  version = "7.6.2"  # Replace with the desired version
#  # Replace with the desired version
#
#  client_name         = var.client_name
#  environment         = var.environment
#  stack               = var.stack
#  location            = module.azure_region.location
#  resource_group_name = module.rg.resource_group_name
#  location_short      = module.azure_region.location_short
#}

module "acr" {
  source  = "claranet/acr/azurerm"
  version = "6.3.0"  # Replace with the desired version
  # Replace with the desired version

  client_name         = var.client_name
  environment         = var.environment
  stack               = var.stack
  location            = module.azure_region.location
  resource_group_name = module.rg.resource_group_name
  sku                 = "Standard"  # Choose your desired SKU

#  logs_destinations_ids = [
#    module.logs.logs_storage_account_id,
#    module.logs.log_analytics_workspace_id
#  ]


  location_short = module.azure_region.location_short
  logs_destinations_ids = []
}
