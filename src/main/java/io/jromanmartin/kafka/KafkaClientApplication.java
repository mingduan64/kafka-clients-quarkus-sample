package io.jromanmartin.kafka;

import io.quarkus.runtime.Quarkus;

public class KafkaClientApplication {
    public static void main(String[] args) {
        // Start the Quarkus application
        Quarkus.run(args);
    }
}
